import { createStore } from 'redux'
import { reducer } from './reducer'
export type { RootState } from './state'
export { createSetOptionAction } from './action'

export let store = createStore(reducer)
