export type RootState = {
  options: Record<string | number, boolean>
}