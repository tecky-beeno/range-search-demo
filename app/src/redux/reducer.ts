import { RootAction } from './action'
import { RootState } from './state'

export let initialState: RootState = { options: {} }

export let reducer = (
  state: RootState = initialState,
  action: RootAction,
): RootState => {
  console.log('[reducer] action:', action)
  switch (action.type) {
    case 'setOption':
      return {
        options: {
          ...state.options,
          [action.id]: action.value,
        },
      }
    default:
      return state
  }
}
