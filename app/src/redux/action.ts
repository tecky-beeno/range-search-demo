export function createSetOptionAction(id: string | number, value: boolean) {
  return {
    type: 'setOption' as const,
    id,
    value,
  }
}

export type RootAction = ReturnType<typeof createSetOptionAction>

