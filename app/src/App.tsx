import React, { useEffect, useState } from 'react'
import logo from './logo.svg'
import './App.css'
import { useDispatch, useSelector } from 'react-redux'
import { store, RootState, createSetOptionAction } from './redux/store'

type Range = {
  min?: number
  max?: number
}

function App() {
  const rangeList: Range[] = [
    { max: 300 },
    { min: 300, max: 450 },
    { min: 450, max: 650 },
    { min: 650 },
  ]

  // const [selectedRangeList, setSelectedRangeList] = useState<Range[]>([])

  const selector = (state: RootState): Range[] => {
    return rangeList.filter(range => {
      const id = rangeToId(range)
      return state.options[id]
    })
  }
  const selectedRangeList = useSelector(selector)

  const [requireAir, setRequireAir] = useState(false)

  const [result, setResult] = useState('')

  async function search() {
    let params = new URLSearchParams()
    params.set('rangeList', JSON.stringify(selectedRangeList))
    params.set('air', requireAir ? 'true' : 'false')
    const res = await fetch('http://127.0.0.1:8100/site?' + params.toString())
    const json = await res.json()
    setResult(JSON.stringify(json, null, 2))
  }

  const dispatch = useDispatch()

  function reset() {
    rangeList.forEach(range => {
      const id = rangeToId(range)
      dispatch(createSetOptionAction(id, false))
    })
  }

  return (
    <div className="App">
      <div>
        {rangeList.map((range, i) => (
          <Option key={i} min={range.min} max={range.max} />
        ))}
        <p>selected: {JSON.stringify(selectedRangeList)}</p>
        <div>
          <label htmlFor="air">require air conditioner</label>
          <input
            type="checkbox"
            id="air"
            onChange={e => setRequireAir(e.target.checked)}
          />
          {requireAir ? 'yes' : 'no'}
        </div>
        <button onClick={search}>search</button>
        <button onClick={reset}>reset</button>
        <pre>
          <code>{result}</code>
        </pre>
      </div>
    </div>
  )
}

function rangeToId({ min, max }: Range): string {
  return `range:${min}-${max}`
}

function Option(props: {
  min?: number
  max?: number
  // onChange: (selected: boolean) => void
}) {
  const { max, min } = props
  const dispatch = useDispatch()
  const id = rangeToId(props)
  const selected = useSelector((state: RootState) => state.options[id])
  function setSelected(value: boolean) {
    dispatch(createSetOptionAction(id, value))
    // store.dispatch(setOption(id, value))
  }
  // const [selected, setSelected] = useState(false)
  return (
    <div
      style={{
        border: selected ? 'orange 1px solid' : 'black 1px solid',
        display: 'inline-block',
        padding: '0.25em',
        borderRadius: '0.5em',
        margin: '0.25em',
        userSelect: 'none',
      }}
      onClick={() => {
        setSelected(!selected)
        // onChange(!selected)
      }}
    >
      {!min ? (
        <>below {max} inches</>
      ) : !max ? (
        <>above {min} inches</>
      ) : (
        <>
          {min} - {max} inches
        </>
      )}
    </div>
  )
}

function useSelectorDemo() {
  const selector = (state: any) => state.auth.username

  const username = useSelector(selector)

  // const [username, setUsername] = useState(selector(store.getState()))
  // useEffect(() => {
  //   const onChange = () => {
  //     const newUsername = selector(store.getState())
  //     if (newUsername !== username) {
  //       setUsername(newUsername)
  //     }
  //   }
  //   let unsubscribe = store.subscribe(onChange)
  //   return () => {
  //     unsubscribe()
  //   }
  // }, [])
}

export default App
