import * as Knex from 'knex'

export async function seed(knex: Knex): Promise<void> {
  await knex.transaction(async knex => {
    // Deletes ALL existing entries
    await knex('site').del()

    // Inserts seed entries
    const items = [
      { size: 150 },

      { size: 299 },
      { size: 300 },
      { size: 301 },

      { size: 449 },
      { size: 450 },
      { size: 451 },

      { size: 649 },
      { size: 650 },
      { size: 651 },

      { size: 750 },
    ]
    await knex('site').insert([
      ...items.map(item => ({ ...item, air: true })),
      ...items.map(item => ({ ...item, air: false })),
    ])
  })
}
