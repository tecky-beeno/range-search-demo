import express from 'express'
import { knex } from './knex'

const router = express.Router()

export default router

type Range = {
  min?: number
  max?: number
}

type Site = {
  size: number
}

router.get('/site', (req, res) => {
  const rangeList: Range[] = JSON.parse(req.query.rangeList as any)
  const requireAir = JSON.parse(req.query.air as any)
  console.log('rangeList:', rangeList)
  console.log({ requireAir })

  let sizeQuery = knex.from<Site>('site').select('id')
  rangeList.forEach(({ min, max }) => {
    if (min && max) {
      sizeQuery = sizeQuery.orWhereBetween('size', [min, max])
    } else if (min) {
      sizeQuery = sizeQuery.orWhere('size', '>=', min)
    } else if (max) {
      sizeQuery = sizeQuery.orWhere('size', '<=', max)
    }
  })

  let airQuery = knex.from<Site>('site').select('id')
  if(requireAir){
    airQuery = airQuery.where('air',true)
  }

  const query = knex.from<Site>('site').select('*')
    .whereIn('id', knex.intersect(sizeQuery,airQuery))
  console.log('sql:', query.toSQL())

  query.then(result => {
    console.log('result:', result)
    res.json({ data: { result } })
  })
})
