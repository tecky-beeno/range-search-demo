import Knex from 'knex'
import configs = require('./knexfile')
const mode = process.env.NODE_ENV || 'development'

export const knex = Knex((configs as any)[mode])
