import * as Knex from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.alterTable('site', t => {
    t.boolean('air')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.alterTable('site', t => {
    t.dropColumn('air')
  })
}
