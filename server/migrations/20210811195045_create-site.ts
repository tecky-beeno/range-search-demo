import * as Knex from 'knex'

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable('site', t => {
    t.increments()
    t.double('size')
  })
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable('site')
}
