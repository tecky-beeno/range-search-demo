console.log('site.js')

let main = document.querySelector('main')

async function loadData(){
  main.innerHTML = '<p>Loading...</p>'
  let res = await fetch('/site')
  let data = await res.json()
  console.log('data:', data)
  if (typeof data === 'object') {
    console.table(data)
  }
  main.innerHTML = '<p>Coming Soon...</p>'
}

loadData()
