import express from 'express'
import cors from 'cors'
import expressSession from 'express-session'
import * as runningAt from 'listening-on'
import { join, resolve } from 'path'
import { config } from 'dotenv'
import { router } from './router'
config()

const app = express()

app.use(cors())

app.use(express.static('public'))

app.use(express.json() as any)
app.use(express.urlencoded({ extended: false }) as any)

const session = expressSession({
  secret: process.env.SESSION_SECRET || Math.random().toString(36),
  saveUninitialized: true,
  resave: true,
})
app.use(session)

app.use((req, res, next) => {
  if (req.method === 'GET') {
    console.log(req.method, req.url)
  } else {
    console.log(req.method, req.url, req.body)
  }
  next()
})

app.use(router)

app.use((req, res) => {
  res.status(404).sendFile(resolve(join('public', '404.html')))
})

const PORT = +process.env.PORT! || 8100

app.listen(PORT, () => {
  runningAt.print(PORT)
})
